import * as types from './mutation_types'
import api from '../api'

export const actionSetLang = ({ commit }, lang) => {
  commit(types.SETLANG, lang)
}

export const actionGetShops = async ({ commit }, payload) => {
  const urlEnd = '/shops'
  const type = 'get'
  const response = await api.asyncRequest(urlEnd, type, payload)

  return response
}
