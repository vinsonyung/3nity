import * as types from './mutation_types.js'

// state
export const state = {
  lang: 'tc',
  newsList: {
    'tc': [
      {
        'route': '/news/lacoste/10',
        'image': '/static/images/news/lacoste/10/1.jpg',
        'header': '經典變革',
        'description': '踏入夏天，不論男女都要為自己增添新衣服，鞋履的配塔往往是穿搭成功與否的關鍵。',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/butterfly-twists/8',
        'image': '/static/images/news/butterfly_twists/8/1.jpg',
        'header': 'Butterfly Twists春夏新推',
        'description': '英國品牌Butterfly Twists近期跟fashion icon Harley V Newton (HVN) 推出聯乘系列，以心心圖案為主打，更配上粉色系迎接夏日的來臨。',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/butterfly-twists/7',
        'image': '/static/images/news/butterfly_twists/7/1.jpg',
        'header': 'Butterfly Twists春夏新推',
        'description': '英國品牌Butterfly Twists愈來愈受女士歡迎，不論上班一族還是輕便逛街的年輕人都不約而同地走進Butterfly Twists的世界。愛美是女孩子的天性，開心買鞋，唔開心買鞋，正好表達了一切。',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/lacoste/9',
        'image': '/static/images/news/lacoste/9/1.jpg',
        'header': 'LACOSTE的經典變革',
        'description': '法國品牌Lacoste一向以簡約見稱，而當中一直大受歡迎的當然是純白色的休閒鞋。今季品牌特意為大熱款式添新意，於Graduate鞋款增添灰色的選擇，同時加上女生最愛的粉紅色，倍添可愛。',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/butterfly-twists/6',
        'image': '/static/images/news/butterfly_twists/6/1.jpg',
        'header': '農曆新年之選  紅鞋子Harper',
        'description': '農曆新年將至，街上各式各樣的新年裝飾已傾巢而出，新年的氣氛亦越見濃烈。各個地方大小的商場亦滲透著強烈的購物慾，大家都忙著為新一年添置新裝。故此，Butterfly Twists同樣為大家準備了最適合農曆新年穿上的鞋履，讓大家於新一年開始即搶盡眼球。',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/lacoste/8',
        'image': '/static/images/news/lacoste/8/1.jpg',
        'header': '情人節之選 送她/他一雙來自法國的鞋',
        'description': '聖誕過後，又來到情人節了！時間過得真快！又是時候想想送甚麼給另一半了！近年流行波鞋風，不少女士也加入了波鞋行列。今個情人節不妨買對來自法國的鞋履給另一半，更能同時為自己買一雙，成為情侶鞋，為另一半製造一個低調的浪漫！',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/sprayground/9',
        'image': '/static/images/news/sprayground/9/1.jpg',
        'header': 'Sprayground 另類創意 速遞背包',
        'description': '新一年開始，當然要準備新事物，以創意為品牌宗旨的Sprayground 當然會為大家帶來別開新面的背包，以另類的設計吸引大家眼球，在全新一年成為矚目焦點，和大家一同成為最新潮人！',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/lacoste/7',
        'image': '/static/images/news/lacoste/7/1.jpg',
        'header': 'Lacoste 與你尋找兒時回憶 全新魔術貼鞋履',
        'description': '長大後，穿上各式各樣的鞋履，款式新穎顏色多變。但，閒時看到學生們，又有否想起在昔日的運動場上那對魔術貼白球鞋呢？這對魔術貼球鞋想當年是學生們的必備鞋款。想不到這個「兒時回憶」，在新一季成為讓大家趨之若鶩的潮鞋，Lacoste亦和大家一同尋找這個兒時回憶，重回魔術貼球鞋的懷抱。',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/sprayground/8',
        'image': '/static/images/news/sprayground/8/1.jpg',
        'header': '冬季好伴侶 Sprayground特別版型格大褸及背包',
        'description': '轉眼之間，已經來到一年中的尾聲，天氣亦逐漸變冷，慢慢踏入冬季。說到冬季，又怎能少了一件有型但又保暖的大褸。Sprayground 於每年的冬季均會推出特別版的大褸，讓一眾粉絲於冬季能溫暖渡過。今年冬天，Sprayground 更特意準備了同系列的大褸與背包，務求讓粉絲們型過冬季。',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/sprayground/7',
        'image': '/static/images/news/sprayground/7/1.jpg',
        'header': '秋風起 登山露營好時機',
        'description': '秋風已漸起，平時出外已經需要添上薄外衣或需穿上長袖的衣物，相信各位好動之人亦慢慢從各種水上活動，逐漸轉移向陸上發展，行山、露營或是野餐的意慾亦隨之蔓延。陸上活動最基本的裝備想必是背包，Sprayground 早已為你準備，讓人輕鬆活動去。',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/butterfly-twists/5',
        'image': '/static/images/news/butterfly_twists/5/1.jpg',
        'header': '戶外活動好伴侶 Robyn',
        'description': '來自英國的Butterfly Twists，創立短短數年，以簡單舒服及方便攜帶聞名，更已獲得不少國際獎項，成為不少女士們的心頭好。Butterfly Twists 亦不滿足於此，積極推出各種的新鞋款，讓女士們能有更多不同的選擇，方便於各種活動或場合穿上，同時亦不失時尚。',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/lacoste/6',
        'image': '/static/images/news/lacoste/6/1.jpg',
        'header': 'Lacoste最新秋裝 Chaumont 法式時尚',
        'description': 'Lacoste創辦人René Lacoste當年以「網球場上的鱷魚」為始成立了品牌，品牌成立以來一直以簡約時尚為品牌的宗旨。數十年過去，Lacoste 的每一款鞋履仍從René Lacoste的設計精髓中汲取靈感，繼承其城市生活風格及冒險精神。',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/sprayground/6',
        'image': '/static/images/news/sprayground/6/1.jpg',
        'header': 'Sprayground注入亞洲元素 - 平衡之道',
        'description': 'Sprayground自2010年創立以來，一直為大家帶來以與別不同的背包的宗旨，勢要打破傳統背包的款式。故此，每一次Sprayground都為大家設計不少玩味十足的背包，更與不少動漫合作，推出限量背包。今次，Sprayground再次大膽加入新元素，注入亞洲元素，以陰陽為最新背包的主題。',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/butterfly-twists/4',
        'image': '/static/images/news/butterfly_twists/4/1.jpg',
        'header': 'Butterfly Twists 讓你閃耀全場',
        'description': '在香港，無論是學生或是工作的女性們，生活都非常忙碌，每天都需要完成形形式式的任務及面對不同的挑戰。在這忙碌的生活中，女士們總會找到屬於自己的時間，放鬆自己，可能是和三五知己聊天，亦可能是和一班好友在周末舉行party，實行 work-life balance。',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/lacoste/5',
        'image': '/static/images/news/lacoste/5/1.jpg',
        'header': 'Lacoste最新亮眼鞋款Explorateur Sport',
        'description': '法國品牌Lacoste 自1933年成立以來，以簡約時尚為品牌的宗旨，不怕時間的洗禮，無懼歲月的變化，打破時間的束縛，創作出無數對經典鞋履，而且時尚與實用兼備。故此，Lacoste 一直在大家心中佔一席位，成為不少人的愛好品牌之一。',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/lacoste/4',
        'image': '/static/images/news/lacoste/4/1.jpg',
        'header': 'Lacoste奪目之銀',
        'description': '經典法國品牌Lacoste始於1933年，由發源人"Rene Lacoste" 所創立。當年，Rene Lacoste是20年代最最矚目的網球選手，創造了7次全球網球比賽連勝的紀錄，更是1926年和1927年的全球網球總冠軍。因此，當時被人稱為"網球場上的鱷魚"，品牌的logo 亦由此而來。而自1933年起，Lacoste 本著以超越時尚與潮流，致力成為不受時間所約束為品牌的宗旨。憑著品牌的堅持，並以其優越的品質和優雅的設計，成就今天國際知名的品牌。',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/sprayground/5',
        'image': '/static/images/news/sprayground/5/1.jpg',
        'header': '與屬於你的Lips 上學去',
        'description': '暑假來去匆匆，轉眼之間又快要到上學的日子，是時候為新學年準備上學的物品，而當中每天背著書本、文具的書包尤為重要，是陪伴每天上下課的重要拍檔！Sprayground已特意為一眾莘莘學子準備好如此重要的好拍擋，推出特別的嘴唇系列。',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/sprayground/4',
        'image': '/static/images/news/sprayground/4/1.jpg',
        'header': '背上Sprayground 與Emoji一同大冒險！',
        'description': 'Sprayground繼推出Marvel蜘蛛俠及Minions大受歡迎之後，Sprayground再次推出crossover背包！今次，是和大家每天都會接觸到的Emoji合作，配合即將上映的《Emoji大冒險》電影，推出限量版的Emoji背包！',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/butterfly-twists/3',
        'image': '/static/images/news/butterfly_twists/3/1.jpg',
        'header': 'Butterfly Twists 經典系列Mya 時尚芭蕾舞鞋',
        'description': '現代女性生活急速忙碌，經常要同時間處理不同時務，更不時要出席各種場合，而且需穿上高跟鞋以示尊重，有見及此Butterfly Twists在成立之時，便以可摺疊的芭蕾舞鞋作賣點，讓女士們帶上一對方便換的鞋履，輕鬆面對不同活動！',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/lacoste/3',
        'image': '/static/images/news/lacoste/3/1.jpg',
        'header': 'Lacoste 經典白鞋 夏日的配搭好伴侶',
        'description': '暑假來臨，各種五顏六色的衣飾也紛紛出籠，迎接夏天，在這個美好的時光爭妍鬥麗。可是，假若身上有太多不同顏色的配搭，有時未免顯得有點俗氣，此時最好用一對簡約的鞋履平衝整個造型。Lacoste 一直以來以簡單且耐看的款式吸引大眾，當中的白色鞋履更是大家的心水之選，最適合配搭各種造型。',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/sprayground/3',
        'image': '/static/images/news/sprayground/3/1.jpg',
        'header': 'Minions迷你兵團 + 蜘蛛俠@Sprayground 共同強勢回歸！',
        'description': '暑假到來，想必大家最近一定非常忙碌，因為一套又一套異常吸引的大電影接踵而來！當中Minions 迷你兵團及蜘蛛俠老少咸宜，是不少人的必看之選。Sprayground 深知大家對他們的熱愛，於電影上映期間再次和Minions 迷你兵團及蜘蛛俠 crossover 推出限量版背囊，滿足粉絲們的心！',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/butterfly-twists/2',
        'image': '/static/images/news/butterfly_twists/2/thumbnail.jpg',
        'header': 'OL必備 Butterfly Twists經典鞋款 Olivia',
        'description': 'Butterfly Twists 可摺疊的鞋履當中，以鞋款Olivia 最為受歡迎，Olivia 共有四種顏色，每一款都由兩種顏色配襯而成，分別是珊瑚襯鴿子灰、乳白襯粉藍、乳白襯黑及由兩種不同深度的黑色襯搭。Olivia 以不同顏色的配襯，增加鞋履的可觀度，加能使鞋履更看搶眼，而幾款不同顏色的配襯適合不同類形的女士。',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/bucketfeet/2',
        'image': '/static/images/news/bucketfeet/2/thumbnail.jpg',
        'header': '凝視的獅子 Bucketfeet非洲風 slip on',
        'description': '美國品牌Bucketfeet 創立而來不斷和世界各地的藝術家合作，創作無數對與別不同的鞋履，將世界上各種藝術文化帶給大家。來到2017年，Bucketfeet繼續與藝術家們合作，頭炮的中國風陶瓷圖案Chinese Porcelain大受歡迎，而繼中國風後，來自非洲的野生動物亦隆重登場',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/sprayground/2',
        'image': '/static/images/news/sprayground/2/thumbnail.jpg',
        'header': 'Sprayground X 賓妮兔家族 太空對戰',
        'description': 'Sprayground 是次推出的 crossover 系列名為Spacejam Good VS Evil，是次crossover 以太空為主題，以太空背景配合由極受歡迎的賓妮兔家族及遊戲Smashup 邪惡人物對疊，雙方如箭在弦，大戰一觸即發，準備來一場生死之戰。另外，更於背囊上襯上Sprayground背囊中的特點 — 飛天雙翼，而且吻合飛上太空的意念',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/butterfly-twists/1',
        'image': '/static/images/news/butterfly_twists/1/1.png',
        'header': 'Butterfly Twists 新款頭炮 黃麻編織 GIGI',
        'description': '英國品牌Butterfly Twists 自今年開始進行變革，以全新形象示人，新的logo 以線條代表品牌蝴蝶的象徵，較過去更為摩登。隨著變革，Butterfly Twists 將會帶來更多不同款式的鞋履，新鞋款的頭炮便在今個復活節隆重推出',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/bucketfeet/1',
        'image': '/static/images/news/bucketfeet/1/2.jpg',
        'header': '融入中國風 Bucketfeet 瓷器slip on',
        'description': 'Bucketfeet 今次為大家帶來具有中國元素的鞋履名為Chinese Porcelain由來自美國的Austin 所設計，Austin 是一位時尚織品的設計師，在求學時期曾經到亞洲學習，更學習製作傳統的工藝作品，亦因此引發她對中國傳統文化的喜愛，更創作了不少相關的作品。',
        'tag': 'Bucketfeet'
      },
      {
        'route': '/news/lacoste/1',
        'image': '/static/images/news/lacoste/1/thumbnail.jpg',
        'header': 'Lacoste SS17 全新鞋款 黑白兩鱷',
        'description': 'Lacoste於新一季增添新成員，推出富有法式簡約味道的LT SPIRIT ELITE。LT SPIRIT ELITE外觀一如Lacoste以往的設計簡潔乾淨，以品牌經典綠色鱷魚logo配上白色透氣網層鞋身，透氣及輕便度同時大增，穿上後更帶來舒適的感覺，踏出的每一步都能釋放腳掌的壓力。',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/sprayground/1',
        'image': '/static/images/news/sprayground/1/thumbnail.jpg',
        'header': 'Sprayground Smartpack',
        'description': '來到新一季，Sprayground 再次帶來新款背囊，今次更照顧一眾電子界朋友，方便大家攜帶所需物件，輕鬆踏上道路。Sprayground 今次推出的背囊名為Smartpack，袋身只有1.5吋厚，，是目前最薄的時尚旅行背囊之一。Smartpack 袋身雖然極為纖薄，卻內有乾坤，最大的內層能擺放各種不同的手提電腦，另外外層是一個專為iPad 而設的間格，方便需攜帶各種電子產品的現代人，不論是上班眾還是學生們。',
        'tag': 'Sprayground'
      }
    ],
    'sc': [
      {
        'route': '/news/lacoste/10',
        'image': '/static/images/news/lacoste/10/1.jpg',
        'header': '经典变革',
        'description': '踏入夏天，不论男女都要为自己增添新衣服，鞋履的配塔往往是穿搭成功与否的关键。',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/butterfly-twists/8',
        'image': '/static/images/news/butterfly_twists/8/1.jpg',
        'header': 'Butterfly Twists春夏新推',
        'description': '英国品牌Butterfly Twists近期跟fashion icon Harley V Newton (HVN) 推出联乘系列，以心心图案为主打，更配上粉色系迎接夏日的来临。',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/butterfly-twists/7',
        'image': '/static/images/news/butterfly_twists/7/1.jpg',
        'header': 'Butterfly Twists春夏新推',
        'description': '英国品牌Butterfly Twists愈来愈受女士欢迎，不论上班一族还是轻便逛街的年轻人都不约而同地走进Butterfly Twists的世界。爱美是女孩子的天性，开心买鞋，唔开心买鞋，正好表达了一切。',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/lacoste/9',
        'image': '/static/images/news/lacoste/9/1.jpg',
        'header': 'LACOSTE的经典变革',
        'description': '法国品牌Lacoste一向以简约见称，而当中一直大受欢迎的当然是纯白色的休闲鞋。今季品牌特意为大热款式添新意，于Graduate鞋款增添灰色的选择，同时加上女生最爱的粉红色，倍添可爱。',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/butterfly-twists/6',
        'image': '/static/images/news/butterfly_twists/6/1.jpg',
        'header': '农历新年之选 红鞋子Harper',
        'description': '农历新年将至，街上各式各样的新年装饰已倾巢而出，新年的气氛亦越见浓烈。各个地方大小的商场亦渗透着强烈的购物欲，大家都忙着为新一年添置新装。故此，Butterfly Twists同样为大家准备了最适合农历新年穿上的鞋履，让大家于新一年开始即抢尽眼球。',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/lacoste/8',
        'image': '/static/images/news/lacoste/8/1.jpg',
        'header': '情人节之选 送她/他一双来自法国的鞋',
        'description': '圣诞过后，又来到情人节了！时间过得真快！又是时候想想送什么给另一半了！近年流行波鞋风，不少女士也加入了波鞋行列。今个情人节不妨买对来自法国的鞋履给另一半，更能同时为自己买一双，成为情侣鞋，为另一半制造一个低调的浪漫！',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/sprayground/9',
        'image': '/static/images/news/sprayground/9/1.jpg',
        'header': 'Sprayground 另类创意 速递背包',
        'description': '新一年开始，当然要准备新事物，以创意为品牌宗旨的Sprayground 当然会为大家带来别开新面的背包，以另类的设计吸引大家眼球，在全新一年成为瞩目焦点，和大家一同成为最新潮人！',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/lacoste/7',
        'image': '/static/images/news/lacoste/7/1.jpg',
        'header': 'Lacoste 与你寻找儿时回忆 全新魔术贴鞋履',
        'description': '长大后，穿上各式各样的鞋履，款式新颖颜色多变。但，闲时看到学生们，又有否想起在昔日的运动场上那对魔术贴白球鞋呢？这对魔术贴球鞋想当年是学生们的必备鞋款。想不到这个「儿时回忆」，在新一季成为让大家趋之若鹜的潮鞋，Lacoste亦和大家一同寻找这个儿时回忆，重回魔术贴球鞋的怀抱。',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/sprayground/8',
        'image': '/static/images/news/sprayground/8/1.jpg',
        'header': '冬季好伴侣Sprayground特别版型格大褛及背包',
        'description': '转眼之间，已经来到一年中的尾声，天气亦逐渐变冷，慢慢踏入冬季。说到冬季，又怎能少了一件有型，但又保暖的大褛。Sprayground于每年的冬季均会推出特别版的大褛，让一众粉丝于冬季能温暖渡过。今年冬天，Sprayground更特意准备了同系列的大外壳与背包，务求让粉丝们过去冬季。',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/sprayground/7',
        'image': '/static/images/news/sprayground/7/1.jpg',
        'header': '秋风起 登山露营好时机',
        'description': '秋风已渐起，平时出外已经需要添上薄外衣或需穿上长袖的衣物，相信各位好动之人亦慢慢从各种水上活动，逐渐转移向陆上发展，行山、露营或是野餐的意欲亦随之蔓延。陆上活动最基本的装备想必是背包，Sprayground 早已为你准备，让人轻松活动去。 ',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/butterfly-twists/5',
        'image': '/static/images/news/butterfly_twists/5/1.jpg',
        'header': '户外活动好伴侣 Robyn',
        'description': '来自英国的Butterfly Twists，创立短短数年，以简单舒服及方便携带闻名，更已获得不少国际奖项，成为不少女士们的心头好。 Butterfly Twists 亦不满足于此，积极推出各种的新鞋款，让女士们能有更多不同的选择，方便于各种活动或场合穿上，同时亦不失时尚。 ',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/lacoste/6',
        'image': '/static/images/news/lacoste/6/1.jpg',
        'header': 'Lacoste最新秋装 Chaumont 法式时尚',
        'description': 'Lacoste创办人René Lacoste当年以「网球场上的鳄鱼」为始成立了品牌，品牌成立以来一直以简约时尚为品牌的宗旨。数十年过去，Lacoste 的每一款鞋履仍从René Lacoste的设计精髓中汲取灵感，继承其城市生活风格及冒险精神。 ',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/sprayground/6',
        'image': '/static/images/news/sprayground/6/1.jpg',
        'header': 'Sprayground注入亚洲元素 - 平衡之道',
        'description': 'Sprayground自2010年创立以来，一直为大家带来以与别不同的背包的宗旨，势要打破传统背包的款式。故此，每一次Sprayground都为大家设计不少玩味十足的背包，更与不少动漫合作，推出限量背包。今次，Sprayground再次大胆加入新元素，注入亚洲元素，以阴阳为最新背包的主题。 ',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/butterfly-twists/4',
        'image': '/static/images/news/butterfly_twists/4/1.jpg',
        'header': 'Butterfly Twists 让你闪耀全场',
        'description': '在香港，无论是学生或是工作的女性们，生活都非常忙碌，每天都需要完成形形式式的任务及面对不同的挑战。在这忙碌的生活中，女士们总会找到属于自己的时间，放松自己，可能是和三五知己聊天，亦可能是和一班好友在周末举行party，实行 work-life balance。 ',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/lacoste/5',
        'image': '/static/images/news/lacoste/5/1.jpg',
        'header': 'Lacoste最新亮眼鞋款Explorateur Sport',
        'description': '法国品牌Lacoste 自1933年成立以来，以简约时尚为品牌的宗旨，不怕时间的洗礼，无惧岁月的变化，打破时间的束缚，创作出无数对经典鞋履，而且时尚与实用兼备。故此，Lacoste 一直在大家心中占一席位，成为不少人的爱好品牌之一。 ',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/lacoste/4',
        'image': '/static/images/news/lacoste/4/1.jpg',
        'header': 'Lacoste夺目之银',
        'description': '经典法国品牌Lacoste始于1933年，由发源人"Rene Lacoste" 所创立。当年，Rene Lacoste是20年代最最瞩目的网球选手，创造了7次全球网球比赛连胜的纪录，更是1926年和1927年的全球网球总冠军。因此，当时被人称为"网球场上的鳄鱼"，品牌的logo 亦由此而来。而自1933年起，Lacoste 本着以超越时尚与潮流，致力成为不受时间所约束为品牌的宗旨。凭着品牌的坚持，并以其优越的品质和优雅的设计，成就今天国际知名的品牌。 ',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/sprayground/5',
        'image': '/static/images/news/sprayground/5/1.jpg',
        'header': '与属于你的Lips 上学去',
        'description': '暑假来去匆匆，转眼之间又快要到上学的日子，是时候为新学年准备上学的物品，而当中每天背著书本、文具的书包尤为重要，是陪伴每天上下课的重要拍档！ Sprayground已特意为一众莘莘学子准备好如此重要的好拍挡，推出特别的嘴唇系列。 ',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/sprayground/4',
        'image': '/static/images/news/sprayground/4/1.jpg',
        'header': '背上Sprayground 与Emoji一同大冒险！ ',
        'description': 'Sprayground继推出Marvel蜘蛛侠及Minions大受欢迎之后，Sprayground再次推出crossover背包！今次，是和大家每天都会接触到的Emoji合作，配合即将上映的《Emoji大冒险》电影，推出限量版的Emoji背包！ ',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/butterfly-twists/3',
        'image': '/static/images/news/butterfly_twists/3/1.jpg',
        'header': 'Butterfly Twists 经典系列Mya 时尚芭蕾舞鞋',
        'description': '现代女性生活急速忙碌，经常要同时间处理不同时务，更不时要出席各种场合，而且需穿上高跟鞋以示尊重，有见及此Butterfly Twists在成立之时，便以可折叠的芭蕾舞鞋作卖点，让女士们带上一对方便换的鞋履，轻松面对不同活动！ ',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/lacoste/3',
        'image': '/static/images/news/lacoste/3/1.jpg',
        'header': 'Lacoste 经典白鞋 夏日的配搭好伴侣',
        'description': '暑假来临，各种五颜六色的衣饰也纷纷出笼，迎接夏天，在这个美好的时光争妍斗丽。可是，假若身上有太多不同颜色的配搭，有时未免显得有点俗气，此时最好用一对简约的鞋履平冲整个造型。 Lacoste 一直以来以简单且耐看的款式吸引大众，当中的白色鞋履更是大家的心水之选，最适合配搭各种造型。 ',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/sprayground/3',
        'image': '/static/images/news/sprayground/3/1.jpg',
        'header': 'Minions迷你兵团 + 蜘蛛侠@Sprayground 共同强势回归！ ',
        'description': '暑假到来，想必大家最近一定非常忙碌，因为一套又一套异常吸引的大电影接踵而来！当中Minions 迷你兵团及蜘蛛侠老少咸宜，是不少人的必看之选。 Sprayground 深知大家对他们的热爱，于电影上映期间再次和Minions 迷你兵团及蜘蛛侠 crossover 推出限量版背囊，满足粉丝们的心！ ',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/butterfly-twists/2',
        'image': '/static/images/news/butterfly_twists/2/thumbnail.jpg',
        'header': 'OL必备 Butterfly Twists经典鞋款 Olivia',
        'description': 'Butterfly Twists 可折叠的鞋履当中，以鞋款Olivia 最为受欢迎，Olivia 共有四种颜色，每一款都由两种颜色配衬而成，分别是珊瑚衬鸽子灰、乳白衬粉蓝、乳白衬黑及由两种不同深度的黑色衬搭。 Olivia 以不同颜色的配衬，增加鞋履的可观度，加能使鞋履更看抢眼，而几款不同颜色的配衬适合不同类形的女士。 ',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/bucketfeet/2',
        'image': '/static/images/news/bucketfeet/2/thumbnail.jpg',
        'header': '凝视的狮子 Bucketfeet非洲风 slip on',
        'description': '美国品牌Bucketfeet 创立而来不断和世界各地的艺术家合作，创作无数对与别不同的鞋履，将世界上各种艺术文化带给大家。来到2017年，Bucketfeet继续与艺术家们合作，头炮的中国风陶瓷图案Chinese Porcelain大受欢迎，而继中国风后，来自非洲的野生动物亦隆重登场',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/sprayground/2',
        'image': '/static/images/news/sprayground/2/thumbnail.jpg',
        'header': 'Sprayground X 宾妮兔家族 太空对战',
        'description': 'Sprayground 是次推出的crossover 系列名为Spacejam Good VS Evil，是次crossover 以太空为主题，以太空背景配合由极受欢迎的宾妮兔家族及游戏Smashup 邪恶人物对叠，双方如箭在弦，大战一触即发，准备来一场生死之战。另外，更于背囊上衬上Sprayground背囊中的特点 — 飞天双翼，而且吻合飞上太空的意念',
        'tag': 'Sprayground'
      },
      {
        'route': '/news/butterfly-twists/1',
        'image': '/static/images/news/butterfly_twists/1/1.png',
        'header': 'Butterfly Twists 新款头炮 黄麻编织 GIGI',
        'description': '英国品牌Butterfly Twists 自今年开始进行变革，以全新形象示人，新的logo 以线条代表品牌蝴蝶的象征，较过去更为摩登。随着变革，Butterfly Twists 将会带来更多不同款式的鞋履，新鞋款的头炮便在今个复活节隆重推出',
        'tag': 'Butterfly Twists'
      },
      {
        'route': '/news/bucketfeet/1',
        'image': '/static/images/news/bucketfeet/1/2.jpg',
        'header': '融入中国风 Bucketfeet 瓷器slip on',
        'description': 'Bucketfeet 今次为大家带来具有中国元素的鞋履名为Chinese Porcelain由来自美国的Austin 所设计，Austin 是一位时尚织品的设计师，在求学时期曾经到亚洲学习，更学习制作传统的工艺作品，亦因此引发她对中国传统文化的喜爱，更创作了不少相关的作品。 ',
        'tag': 'Bucketfeet'
      },
      {
        'route': '/news/lacoste/1',
        'image': '/static/images/news/lacoste/1/thumbnail.jpg',
        'header': 'Lacoste SS17 全新鞋款 黑白两鳄',
        'description': 'Lacoste于新一季增添新成员，推出富有法式简约味道的LT SPIRIT ELITE。 LT SPIRIT ELITE外观一如Lacoste以往的设计简洁干净，以品牌经典绿色鳄鱼logo配上白色透气网层鞋身，透气及轻便度同时大增，穿上后更带来舒适的感觉，踏出的每一步都能释放脚掌的压力。 ',
        'tag': 'Lacoste'
      },
      {
        'route': '/news/sprayground/1',
        'image': '/static/images/news/sprayground/1/thumbnail.jpg',
        'header': 'Sprayground Smartpack',
        'description': '来到新一季，Sprayground 再次带来新款背囊，今次更照顾一众电子界朋友，方便大家携带所需物件，轻松踏上道路。 Sprayground 今次推出的背囊名为Smartpack，袋身只有1.5吋厚，，是目前最薄的时尚旅行背囊之一。 Smartpack 袋身虽然极为纤薄，却内有乾坤，最大的内层能摆放各种不同的手提电脑，另外外层是一个专为iPad 而设的间格，方便需携带各种电子产品的现代人，不论是上班众还是学生们。 ',
        'tag': 'Sprayground'
      }
    ]
  }
}

// mutations
export const mutations = {
  [types.SETLANG](state, lang) {
    state.lang = lang
  }
}
