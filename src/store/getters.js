export const getLang = state => {
  return state.lang
}

export const getNewsList = state => {
  return state.newsList
}
