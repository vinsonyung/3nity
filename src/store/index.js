import Vue from 'vue'
import Vuex from 'vuex'

import { state, mutations } from './mutations'
import * as getters from './getters'
import * as actions from './actions'

Vue.use(Vuex)

const store = new Vuex.Store({
  strict: true,
  state,
  mutations,
  getters,
  actions
})

export default store
