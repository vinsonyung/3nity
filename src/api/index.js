import Vue from 'vue'

const api = {}

api.timeout = { timeout: 20000 }
api.serverURL = 'http://api.3nityadmin.dev/api'

api.asyncRequest = async (urlEnd, type, payload = {}) => {
  // return await Vue.axios.get(api.serverURL + urlEnd, payload, api.timeout)

  if (type === 'get') {
    return Vue.axios.get(api.serverURL + urlEnd)
  } else if (type === 'post') {
    return Vue.axios.get(api.serverURL + urlEnd)
  }
}

export default api
