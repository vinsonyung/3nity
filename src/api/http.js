import axios from 'axios'

export const HTTP = axios.create({
  baseURL: process.env.API_URL,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'crossDomain': true
  },
  timeout: 20000
})
