import Vue from 'vue'
import VueI18n from 'vue-i18n'

// Import language files
import en from './en'
import tc from './tc'
import sc from './sc'

Vue.use(VueI18n)

let messages = {}

const locales = {
  en: en,
  tc: tc,
  sc: sc
}

// Assign all language data to messages object
Object.keys(locales).forEach(function (lang) {
  messages[lang] = locales[lang]
})

const i18n = new VueI18n({
  locale: localStorage.getItem('lang') ? localStorage.getItem('lang') : 'tc',
  fallbackLocale: 'tc',
  silentTranslationWarn: false,
  messages
})

// Provide $locale to change language
Vue.prototype.$locale = {
  change (lang) {
    i18n.locale = lang
  },
  current () {
    return i18n.locale
  }
}

export default i18n
