import Vue from 'vue'
import Router from 'vue-router'

// Import app components
import Home from '@/pages/Home'
import AboutUs from '@/pages/AboutUs'

import ButterflyTwists from '@/pages/brands/ButterflyTwists'
import CreativeRecreation from '@/pages/brands/CreativeRecreation'
import Lacoste from '@/pages/brands/Lacoste'
import Sprayground from '@/pages/brands/Sprayground'
import Wesc from '@/pages/brands/Wesc'

import Shop3nity from '@/pages/shops/3nity'
import ShopDistributor from '@/pages/shops/Distributor'

import Store from '@/pages/Store'
import ContactUs from '@/pages/ContactUs'
import PrivacyPolicy from '@/pages/PrivacyPolicy'

import Subscription from '@/pages/Subscription'

import Promotion from '@/pages/Promotion'

import News from '@/pages/News'
import NewsBucketfeet1 from '@/pages/news/bucketfeet/1'
import NewsBucketfeet2 from '@/pages/news/bucketfeet/2'

import NewsButterflyTwists1 from '@/pages/news/butterfly_twists/1'
import NewsButterflyTwists2 from '@/pages/news/butterfly_twists/2'
import NewsButterflyTwists3 from '@/pages/news/butterfly_twists/3'
import NewsButterflyTwists4 from '@/pages/news/butterfly_twists/4'
import NewsButterflyTwists5 from '@/pages/news/butterfly_twists/5'
import NewsButterflyTwists6 from '@/pages/news/butterfly_twists/6'
import NewsButterflyTwists7 from '@/pages/news/butterfly_twists/7'
import NewsButterflyTwists8 from '@/pages/news/butterfly_twists/8'

import NewsLacoste1 from '@/pages/news/lacoste/1'
import NewsLacoste2 from '@/pages/news/lacoste/2'
import NewsLacoste3 from '@/pages/news/lacoste/3'
import NewsLacoste4 from '@/pages/news/lacoste/4'
import NewsLacoste5 from '@/pages/news/lacoste/5'
import NewsLacoste6 from '@/pages/news/lacoste/6'
import NewsLacoste7 from '@/pages/news/lacoste/7'
import NewsLacoste8 from '@/pages/news/lacoste/8'
import NewsLacoste9 from '@/pages/news/lacoste/9'
import NewsLacoste10 from '@/pages/news/lacoste/10'

import NewsSprayground1 from '@/pages/news/sprayground/1'
import NewsSprayground2 from '@/pages/news/sprayground/2'
import NewsSprayground3 from '@/pages/news/sprayground/3'
import NewsSprayground4 from '@/pages/news/sprayground/4'
import NewsSprayground5 from '@/pages/news/sprayground/5'
import NewsSprayground6 from '@/pages/news/sprayground/6'
import NewsSprayground7 from '@/pages/news/sprayground/7'
import NewsSprayground8 from '@/pages/news/sprayground/8'
import NewsSprayground9 from '@/pages/news/sprayground/9'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        title: 'home'
      }
    },
    {
      path: '/about-us',
      name: 'AboutUs',
      component: AboutUs,
      meta: {
        title: 'about_us'
      }
    },
    {
      path: '/brands/butterfly-twists',
      name: 'ButterflyTwists',
      component: ButterflyTwists,
      meta: {
        title: 'Butterfly Twists'
      }
    },
    {
      path: '/brands/creative-recreation',
      name: 'CreativeRecreation',
      component: CreativeRecreation,
      meta: {
        title: 'Creative Recreation'
      }
    },
    {
      path: '/brands/lacoste',
      name: 'Locoste',
      component: Lacoste,
      meta: {
        title: 'Locoste'
      }
    },
    {
      path: '/brands/sprayground',
      name: 'Sprayground',
      component: Sprayground,
      meta: {
        title: 'Sprayground'
      }
    },
    {
      path: '/brands/wesc',
      name: 'Wesc',
      component: Wesc,
      meta: {
        title: 'WeSc'
      }
    },
    {
      path: '/shop/3nity',
      name: 'Shop3nity',
      component: Shop3nity
    },
    {
      path: '/shop/distributor',
      name: 'ShopDistributor',
      component: ShopDistributor,
      meta: {
        title: 'distributor'
      }
    },
    {
      path: '/news',
      name: 'News',
      component: News,
      meta: {
        title: 'news'
      }
    },
    {
      path: '/news/bucketfeet/1',
      name: 'NewsBucketfeet1',
      component: NewsBucketfeet1
    },
    {
      path: '/news/bucketfeet/2',
      name: 'NewsBucketfeet2',
      component: NewsBucketfeet2
    },
    {
      path: '/news/butterfly-twists/1',
      name: 'NewsButterflyTwists1',
      component: NewsButterflyTwists1
    },
    {
      path: '/news/butterfly-twists/2',
      name: 'NewsButterflyTwists2',
      component: NewsButterflyTwists2
    },
    {
      path: '/news/butterfly-twists/3',
      name: 'NewsButterflyTwists3',
      component: NewsButterflyTwists3
    },
    {
      path: '/news/butterfly-twists/4',
      name: 'NewsButterflyTwists4',
      component: NewsButterflyTwists4
    },
    {
      path: '/news/butterfly-twists/5',
      name: 'NewsButterflyTwists5',
      component: NewsButterflyTwists5
    },
    {
      path: '/news/butterfly-twists/6',
      name: 'NewsButterflyTwists6',
      component: NewsButterflyTwists6
    },
    {
      path: '/news/butterfly-twists/7',
      name: 'NewsButterflyTwists7',
      component: NewsButterflyTwists7
    },
    {
      path: '/news/butterfly-twists/8',
      name: 'NewsButterflyTwists8',
      component: NewsButterflyTwists8
    },
    {
      path: '/news/lacoste/1',
      name: 'NewsLacoste1',
      component: NewsLacoste1
    },
    {
      path: '/news/lacoste/2',
      name: 'NewsLacoste2',
      component: NewsLacoste2
    },
    {
      path: '/news/lacoste/3',
      name: 'NewsLacoste3',
      component: NewsLacoste3
    },
    {
      path: '/news/lacoste/4',
      name: 'NewsLacoste4',
      component: NewsLacoste4
    },
    {
      path: '/news/lacoste/5',
      name: 'NewsLacoste5',
      component: NewsLacoste5
    },
    {
      path: '/news/lacoste/6',
      name: 'NewsLacoste6',
      component: NewsLacoste6
    },
    {
      path: '/news/lacoste/7',
      name: 'NewsLacoste7',
      component: NewsLacoste7
    },
    {
      path: '/news/lacoste/8',
      name: 'NewsLacoste8',
      component: NewsLacoste8
    },
    {
      path: '/news/lacoste/9',
      name: 'NewsLacoste9',
      component: NewsLacoste9
    },
    {
      path: '/news/lacoste/10',
      name: 'NewsLacoste10',
      component: NewsLacoste10
    },
    {
      path: '/news/sprayground/1',
      name: 'NewsSprayground1',
      component: NewsSprayground1
    },
    {
      path: '/news/sprayground/2',
      name: 'NewsSprayground2',
      component: NewsSprayground2
    },
    {
      path: '/news/sprayground/3',
      name: 'NewsSprayground3',
      component: NewsSprayground3
    },
    {
      path: '/news/sprayground/4',
      name: 'NewsSprayground4',
      component: NewsSprayground4
    },
    {
      path: '/news/sprayground/5',
      name: 'NewsSprayground5',
      component: NewsSprayground5
    },
    {
      path: '/news/sprayground/6',
      name: 'NewsSprayground6',
      component: NewsSprayground6
    },
    {
      path: '/news/sprayground/7',
      name: 'NewsSprayground7',
      component: NewsSprayground7
    },
    {
      path: '/news/sprayground/8',
      name: 'NewsSprayground8',
      component: NewsSprayground8
    },
    {
      path: '/news/sprayground/9',
      name: 'NewsSprayground9',
      component: NewsSprayground9
    },
    {
      path: '/stores',
      name: 'Store',
      component: Store,
      meta: {
        title: 'find_a_store.title'
      }
    },
    {
      path: '/contact-us',
      name: 'ContactUs',
      component: ContactUs,
      meta: {
        title: 'contact_us'
      }
    },
    {
      path: '/privacy-policy',
      name: 'PrivacyPolicy',
      component: PrivacyPolicy,
      meta: {
        title: 'footer.privacy_policy'
      }
    },
    {
      path: '/subscription',
      name: 'Subscription',
      component: Subscription,
      meta: {
        title: 'subscription'
      }
    },
    {
      path: '/promotion',
      name: 'Promotion',
      component: Promotion,
      meta: {
        title: 'subscription'
      }
    }
  ]
})
