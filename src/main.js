// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'

import i18n from './i18n/locales'
import store from './store'

import es6Promise from 'es6-promise'

import '../semantic/dist/semantic.min.js'
import '../semantic/dist/semantic.min.css'

import 'swiper/dist/css/swiper.css'

Vue.config.productionTip = false

Vue.use(Vuex)

es6Promise.polyfill()

// router.beforeEach((to, from, next) => {
//   let title = '3NITY'

//   if (to.meta.title) {
//     title = '3NITY - ' + i18n.t(to.meta.title)
//   }

//   document.title = title

//   next()
// })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  template: '<App/>',
  components: { App }
})
